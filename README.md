# Robert the Constructor

Robert the Constructor is your faithful helper for building code projects!

## Introduction

Robert the Constructor is a build system written in C which aims to make software building as fast, turnkey, and self-contained as possible. While it can be installed system-wide, its primary usage mode is as a project dependency that gets bootstrapped with one simple compiler command. From there, Bob the Constructor can build your software, run tests, or anything else you can think of.

*NOTE:* Any similarity to persons, whether living, dead, or fictional, are purely tongue-in-cheek. Seriously, it's all in good fun!

## Installation

### As a git submodule

First, add the submodule to your repository:

``` sh
git submodule add --branch v1.0 --depth 1 --name bob -- https://gitlab.com/deriamis/robert-the-constructor.git path/to/submodule
```

Next, configure the module to always use a shallow clone:

``` sh
git config -f .gitmodules submodule.path/to/submodule.shallow true
```

Finally, make sure you commit your changes to the git repository and push them to your remote (if necessary) so others will be able to check out the submodule.

Users of your repository will need to clone your repository recursively to populate the submodule:

``` sh
git clone --recurse-submodules http://your.repo.url/repo.git
```

Or, if the repository has already been cloned, pull it to ensure the submodule is present and then initialize the submodule:

``` sh
git pull
git submodule update --init --recursive
```

### System-Wide

## Updating

### Git submodule

First, update the submodule branch:

``` sh
git config -f .gitmodules submodule.:path/to/submodule.branch <branch/tag>
```

Next, pull and update the submodule:

``` sh
git fetch
git submodule sync --recursive
git submodule --init --recursive
```

Finally, commit the updated submodule and push them to your remote.

### System-Wide

## Usage

First, bootstrap the build by running the following (or similar in your compiler's command syntax):

`cc bob.c -o bob`

Robert the Constructor (`bob` to his friends) knows how to do most simple things without you telling him. Out of the box, running `bob build` will generate a `config.h` with some commonly-needed `#define`s and will compile `main.c` in the current directory to a binary named after the directory it's in. If also want to *run* your built code, use `bob run` instead.

If you want more than that, you'll have to tell `bob` what else you need with a [configuration](#configuration).

## Configuration

## Why C?

This is a build system that is intended to build anything.

After the nearly 50 years since its inception, C is the only truly portable and cross-platform language in widespread use. This is because writing a functioning C compiler is easy enough for most programmers to do on their own once they have enough experience. The same cannot be said of C++, which is notoriously difficult to parse and translate; nor can it be said of Rust, which depends on the LLVM framework for binary output. Other languages such as Python, Ruby, Perl, Java, etc. either depend on a VM running bytecode or have language rules that make a full-featured compiler to native code difficult or impossible.

In short, if you want it to compile and run _literally_ anywhere, C is your best bet.

This doesn't mean C is the best programming language in general. I've used many of the languages previously mentioned and enjoy using them. Programming languages are tools, and C is (in my opinion) the best one for this task.

## Acknowledgements

Many thanks to [@tsoding](https://github.com/tsoding), whose [`nob`](https://github.com/tsoding/musializer/blob/master/src/nob.h) project is a major inspiration for this one. In fact, much of this project's code is theirs - though rearranged and modified/updated for C23. Their "recreational coding" livestreams are truly fantastic and helped to encourage me to write my own take on the tool after kicking ideas around in my head for several years. You can watch their livestreams on [Twitch](https://tsoding.github.io/).
